use std::{fs::File, io::BufReader, string::String};

#[derive(Debug)]
pub struct GapBuffer {
    text: String,
    buffer: String,
    cursor: usize,
}

impl GapBuffer {
    pub fn new(text: String) -> GapBuffer {
        GapBuffer {
            text: text,
            buffer: String::new(),
            cursor: 0,
        }
    }

    pub fn len(&self) -> usize {
        self.text.len() + self.buffer.len()
    }

    pub fn insert(&mut self, index: usize, c: char) -> Result<(), &str> {
        println!("1 {:?} index: {}", self, index);
        if index > self.text.len() + self.buffer.len() {
            return Err("Out of bounds");
        }
        if self.cursor < index && index < self.cursor + self.buffer.len() {
            println!("\nMiddle of buffer: {:?}", self);
            self.commit(Some(index - self.cursor - 1));
            println!("After commit: {:?}", self);
            self.buffer.push(c);
            println!("After push: {:?}", self);
            return Ok(());
        }
        if index == self.cursor + self.buffer.len() {
            self.buffer.push(c);
            println!("End of buffer: {:?}", self);
            return Ok(());
        }
        self.commit(None);
        self.cursor = index;
        self.buffer.push(c);
        println!("2 {:?} index: {}", self, index);
        Ok(())
    }

    fn commit(&mut self, index: Option<usize>) {
        match index {
            Some(index) => {
                println!("SEX");
                if self.get_text().is_char_boundary(index) && self.cursor <= self.text.len() {
                    let chars: String = self.buffer.drain(index + 1..).collect();
                    self.text.insert_str(self.cursor, chars.as_str())
                }
                self.cursor = std::cmp::min(self.cursor, self.text.len());
            }
            None => {
                self.text.insert_str(self.cursor, self.buffer.as_str());
                self.buffer.clear();
            }
        }
    }

    pub fn remove(&mut self, index: usize) -> Result<(), usize> {
        self.cursor = std::cmp::min(self.cursor, self.text.len());
        if index == self.cursor + self.buffer.len() - 1 && self.buffer.len() > 0 {
            self.buffer.pop();
            println!("POPPP");
            return Ok(());
        } else if index + 1 > self.len() {
            return Err(index);
        } else {
            println!("{:?}", self);
            self.commit(None);
            println!("COMMITTED AND REMOVING {:?}", self);
            self.text.drain(index..index + 1).next();
            return Ok(());
        }
    }

    pub fn get_text(&self) -> String {
        if self.buffer.len() == 0 {
            return self.text.clone();
        }
        format!(
            "{}{}{}",
            &self.text[..self.cursor],
            self.buffer,
            &self.text[self.cursor..]
        )
    }
}

pub struct Buffer {
    pub lines: Vec<GapBuffer>,
    pub first_visible: isize,
    pub last_visible: isize,
}

impl Buffer {
    pub fn new(lines: std::io::Lines<BufReader<&File>>) -> Buffer {
        let lines = lines.map(|x| GapBuffer::new(x.unwrap())).collect();

        Buffer {
            lines: lines,
            first_visible: 0,
            last_visible: 0,
        }
    }

    pub fn new_line(&mut self, index: usize, line: usize) {
        println!("NEWLNE");
        let old_line = &mut self.lines[line];
        old_line.commit(None);
        let new_text: String = old_line.text.drain(index..).collect();
        self.lines.insert(line + 1, GapBuffer::new(new_text));
    }

    pub fn delete_line(&mut self, index: usize, line: usize) -> Result<(), ()> {
        if line >= self.lines.len() {
            return Err(());
        }
        self.lines[line].commit(None);
        let merged: String = self.lines[line].text.drain(index..).collect();
        self.lines[line - 1].commit(None);
        self.lines[line - 1].text.push_str(merged.as_str());
        self.lines.remove(line);
        Ok(())
    }

    pub fn get_visible(&self) -> Vec<String> {
        self.lines[self.first_visible as usize..]
            .iter()
            .map(|x| x.get_text())
            .collect::<Vec<_>>()
    }

    pub fn scroll(&mut self, mut num_lines: isize) {
        if num_lines + self.first_visible < 0 {
            num_lines = -self.first_visible;
        }

        if num_lines + self.first_visible > self.lines.len() as isize {
            num_lines = self.lines.len() as isize - self.first_visible - 1;
        }

        self.first_visible += num_lines;
        self.last_visible += num_lines;
    }
}
