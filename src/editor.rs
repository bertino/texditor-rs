use crate::buffer;
use clipboard::{self, ClipboardProvider};
use ggez::event::{self, EventHandler};
use ggez::{
    graphics,
    graphics::DrawMode,
    input::keyboard::{KeyCode, KeyMods},
    nalgebra as na, Context, GameResult,
};
use std::{
    cmp, fs,
    io::{self, BufRead, Write},
};

pub struct Editor {
    buffer: buffer::Buffer,
    font: graphics::Font,
    font_width: f32,
    font_height: f32,
    scroll_lines: isize,
    tabs: usize,
    text_color: graphics::Color,
    accent_color: graphics::Color,
    background_color: graphics::Color,
    padding: f32,
    gutter_chars: usize,
    filename: String,
    cursor: (usize, usize),
    clip: Option<clipboard::ClipboardContext>,
}

#[derive(Debug)]
enum MousePos {
    Content((isize, isize)),
    Gutter(isize),
}

fn num_digits(x: usize) -> usize {
    let mut x = x.clone();
    let mut i = 0;
    while {
        x /= 10;
        i += 1;
        x != 0
    } {}
    i
}

impl Editor {
    pub fn new(_ctx: &mut Context, filename: &str) -> Result<Editor, io::Error> {
        let file = Editor::open_file(filename)?;
        let lines = io::BufReader::new(&file).lines();

        let buffer = buffer::Buffer::new(lines);
        Ok(Editor {
            buffer: buffer,
            font: graphics::Font::new(_ctx, "/JetBrainsMono-Regular.ttf").unwrap_or_else(|err| {
                println!("Error opening font file. Using default.\n\n{}", err);
                graphics::Font::default()
            }),
            font_width: 11.,
            font_height: 24.,
            scroll_lines: 3,
            tabs: 4,
            text_color: graphics::Color::new(0.8, 0.8, 0.8, 1.0),
            accent_color: graphics::Color::new(0.8, 0.2, 0.3, 1.0),
            background_color: graphics::Color::new(0.08, 0.08, 0.08, 1.0),
            padding: 8.,
            gutter_chars: 0,
            filename: filename.to_string(),
            cursor: (0, 0),
            clip: match ClipboardProvider::new() {
                Ok(clip) => Some(clip),
                _ => None,
            },
        })
    }

    pub fn init(&mut self, ctx: &mut Context) {
        let text = graphics::TextFragment::new(("x".repeat(10000), self.font, self.font_height));
        self.font_width = graphics::Text::new(text).width(ctx) as f32 / 10000.;
        if self.buffer.lines.len() == 0 {
            self.buffer
                .lines
                .push(buffer::GapBuffer::new(String::from("")));
        }
    }

    fn open_file(filename: &str) -> io::Result<fs::File> {
        fs::OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(filename)
    }

    fn save_file(&mut self) -> std::io::Result<()> {
        let mut file = fs::File::create(&self.filename)?;
        for line in &self.buffer.lines {
            file.write_all((line.get_text() + "\n").as_bytes())?;
        }
        Ok(())
    }

    fn paste(&mut self, ctx: &mut Context) {
        match &mut self.clip {
            Some(clip) => {
                let contents = clip.get_contents().unwrap_or(String::from(""));
                println!("{}", contents);
                for line in contents.lines() {
                    for ch in line.chars() {
                        self.buffer.lines[self.cursor.1]
                            .insert(self.cursor.0, ch)
                            .expect("Not good");
                        self.cursor.0 += 1;
                    }
                    self.buffer.new_line(self.cursor.0, self.cursor.1);
                    self.set_cursor(ctx, (0, self.cursor.1 as isize + 1));
                }
                self.text_input_event(ctx, '\u{8}');
            }
            None => (),
        }
    }

    fn get_mouse_pos(&self, x: f32, y: f32) -> MousePos {
        let col: isize =
            ((x - 3. * self.padding) / self.font_width) as isize - self.gutter_chars as isize;

        if col < 0 {
            return MousePos::Gutter((y / self.font_height) as isize);
        }

        let x = col;
        let y = (y / self.font_height) as isize;
        MousePos::Content((x, y))
    }

    fn constrain_left(&self, target: &mut (isize, isize)) {
        if target.0 == -1 {
            if target.1 >= 0 && self.buffer.first_visible + target.1 > 0 {
                // Scroll up if lines available
                target.1 -= 1;
                let line_index = (target.1 + self.buffer.first_visible) as usize;
                target.0 = self.buffer.lines[line_index].len() as isize;
            } else {
                // Beginning of file
                target.0 = 0;
            }
        }
    }

    fn constrain_up(&mut self, target: &mut (isize, isize)) {
        if target.1 == -1 {
            if self.buffer.first_visible == 0 {
                // Beginning of file

                target.1 = 0;
            } else {
                // Scroll up

                self.buffer.scroll(-1);
                target.1 = 0;
            }
        }
    }

    fn constrain_down(&mut self, target: &mut (isize, isize), ctx: &Context) {
        let (_x, last_visible) = graphics::drawable_size(ctx);
        let last_visible = (last_visible / self.font_height) as isize - 1; // Last visible line

        if target.1 > last_visible && target.1 > 0 {
            // Scroll down if possible
            self.buffer.scroll(1);
            target.1 -= 1;
        }

        let last = self.buffer.lines.len() as isize - self.buffer.first_visible - 1; // Last line of file

        target.1 = cmp::min(target.1, cmp::min(last, last_visible)); // bottom of the file
    }

    fn constrain_right(&mut self, target: &mut (isize, isize)) {
        let last = self.buffer.lines.len() as isize - self.buffer.first_visible - 1; // Last line of file
        let current_line_index = (target.1 + self.buffer.first_visible) as usize;
        let current_line = &self.buffer.lines[current_line_index];

        if target.0 > current_line.len() as isize {
            if target.1 != last && target.1 - (self.cursor.1 as isize) < 1 {
                target.0 = 0;
                target.1 += 1;
            } else {
                target.0 = current_line.len() as isize;
            }
        }
    }

    fn set_cursor(&mut self, ctx: &Context, mut target: (isize, isize)) {
        self.constrain_left(&mut target);
        self.constrain_up(&mut target);
        self.constrain_down(&mut target, ctx);
        self.constrain_right(&mut target);
        self.constrain_down(&mut target, ctx);
        self.cursor = (target.0 as usize, target.1 as usize);
    }
}

impl EventHandler for Editor {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        // Update code here...
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        let lines = self.buffer.lines.len();
        self.gutter_chars = num_digits(lines);
        // graphics::set_window_title(
        //     ctx,
        //     format!(
        //         "Texditor {} - FPS: {:.3} - Gutter: {}",
        //         self.filename,
        //         ggez::timer::fps(ctx),
        //         self.gutter_chars as f32 * self.font_width,
        //     )
        //     .as_str(),
        // );
        graphics::clear(ctx, self.background_color);

        let origin = na::Point2::origin();

        let gutter_background = graphics::Rect::new(
            0.,
            0.,
            self.font_width * self.gutter_chars as f32 + self.padding * 2.,
            1000.,
        );

        let gutter_background = graphics::MeshBuilder::new()
            .rectangle(DrawMode::fill(), gutter_background, graphics::BLACK)
            .build(ctx)?;
        graphics::draw(ctx, &gutter_background, (origin,))?;

        let mut index: usize = 0;

        for line in &self.buffer.get_visible() {
            // Draw line number
            let number = index + self.buffer.first_visible as usize;
            let text =
                graphics::TextFragment::new((format!("{}", number), self.font, self.font_height))
                    .color(self.accent_color);
            let text = graphics::Text::new(text);

            // Offset to right-align the line number
            let offset = (self.gutter_chars - num_digits(number)) as f32 * self.font_width;
            graphics::draw(
                ctx,
                &text,
                (na::Point2::new(
                    self.padding + offset,
                    index as f32 * self.font_height,
                ),),
            )?;

            // Draw line text
            let text = graphics::TextFragment::new((line.clone(), self.font, self.font_height))
                .color(self.text_color);
            let text = graphics::Text::new(text);
            let width = text.width(ctx);

            if width > 22 {
                graphics::set_window_title(
                    ctx,
                    format!(
                        "Texditor {} - FPS: {:.1} - Font width: {} - Text width {}",
                        self.filename,
                        ggez::timer::fps(ctx),
                        self.font_width,
                        width
                    )
                    .as_str(),
                );
            }

            graphics::draw(
                ctx,
                &text,
                (na::Point2::new(
                    3. * self.padding + self.font_width * self.gutter_chars as f32,
                    index as f32 * self.font_height,
                ),),
            )?;
            index += 1;
        }

        // Draw cursor
        let cursor = graphics::Rect::new(
            (self.cursor.0 + self.gutter_chars) as f32 * self.font_width + 3. * self.padding,
            self.cursor.1 as f32 * self.font_height,
            2.,
            self.font_height,
        );

        let cursor = graphics::MeshBuilder::new()
            .rectangle(DrawMode::stroke(2.), cursor, self.text_color)
            .build(ctx)?;

        graphics::draw(ctx, &cursor, (origin,))?;

        graphics::present(ctx)
    }

    fn resize_event(&mut self, ctx: &mut Context, width: f32, height: f32) {
        graphics::set_screen_coordinates(ctx, graphics::Rect::new(0., 0., width, height))
            .expect("Error while resizing");
        self.set_cursor(ctx, (self.cursor.0 as isize, self.cursor.0 as isize));
    }

    fn mouse_wheel_event(&mut self, ctx: &mut Context, _x: f32, y: f32) {
        self.buffer.scroll(self.scroll_lines * -1 * y as isize);
        self.set_cursor(ctx, (self.cursor.0 as isize, self.cursor.0 as isize));
    }

    fn mouse_button_up_event(
        &mut self,
        ctx: &mut Context,
        _button: event::MouseButton,
        x: f32,
        y: f32,
    ) {
        let mouse_pos = self.get_mouse_pos(x, y);
        let target = match mouse_pos {
            MousePos::Content((x, y)) => (x, y),
            MousePos::Gutter(y) => (0, y),
        };

        self.set_cursor(ctx, target);
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: KeyCode,
        keymods: KeyMods,
        _repeat: bool,
    ) {
        let dir: (isize, isize) = match keycode {
            KeyCode::Up => (0, -1),
            KeyCode::Down => (0, 1),
            KeyCode::Left => (-1, 0),
            KeyCode::Right => (1, 0),
            _ => (0, 0),
        };

        if dir != (0, 0) {
            let target = (
                self.cursor.0 as isize + dir.0,
                self.cursor.1 as isize + dir.1,
            );

            self.set_cursor(ctx, target);
        }

        match keycode {
            KeyCode::F4 => {
                if keymods == KeyMods::ALT {
                    event::quit(ctx)
                }
            }
            KeyCode::Q => {
                if keymods == KeyMods::CTRL {
                    event::quit(ctx)
                }
            }
            KeyCode::S => {
                if keymods == KeyMods::CTRL {
                    self.save_file().unwrap_or(());
                }
            }
            KeyCode::V => {
                if keymods == KeyMods::CTRL {
                    self.paste(ctx);
                }
            }
            _ => {}
        };
    }

    fn text_input_event(&mut self, ctx: &mut Context, c: char) {
        let line_index = self.buffer.first_visible as usize + self.cursor.1;
        let line = &mut self.buffer.lines[line_index];
        let mut index = self.cursor.0;
        println!("{:?},    c.len_utf8() {}", c, c.len_utf8());
        if c == '\u{8}' {
            // Backspace
            if index > 0 {
                line.remove(index - 1)
                    .expect("Tried to remove invalid char");
                self.cursor.0 -= 1;
            } else if line_index > 0 {
                self.cursor.1 -= 1;
                self.cursor.0 = self.buffer.lines[self.cursor.1].len();
                self.buffer.delete_line(index, line_index).unwrap_or(());
            }
        } else if c == '\u{7f}' {
            // Delete
            println!("{}, {}", index, line.len());
            if index + 1 <= line.len() {
                line.remove(index).expect("Wrong char deletion");
            } else if index == line.len() {
                self.buffer.delete_line(0, line_index + 1).unwrap_or(());
            }
        } else if c == '\r' {
            self.buffer.new_line(index, line_index);
            self.set_cursor(ctx, (0, self.cursor.1 as isize + 1));
        } else if (c.is_ascii() || (!c.is_ascii() && c.len_utf8() == 1)) && !c.is_ascii_whitespace() && !c.is_ascii_control() || c == ' ' {
            line.insert(index, c).expect("Index gone wrong");
            self.cursor.0 += 1;
        } else if c == '\t' {
            println!("ATAB");
            for _i in 1..self.tabs {
                line.insert(index, ' ').expect("INDEX");
                index += 1;
                self.cursor.0 += 1;
                println!("{:?}", self.cursor);
            }
        }
    }
}
