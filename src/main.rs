use ggez::{conf, event, ContextBuilder};
use std::env;

mod editor;
use editor::Editor;
mod buffer;

fn main() {
    let defaultname = String::from("newfile.txt");
    let filename = env::args().collect::<Vec<_>>();
    let filename = filename.get(1).unwrap_or_else(|| &defaultname);

    // Make a Context.
    let window_conf = conf::WindowSetup {
        title: format!("Texditor: {}", filename).to_owned(),
        samples: conf::NumSamples::Four,
        vsync: true,
        icon: "".to_owned(),
        srgb: true,
    };

    let window_mode = conf::WindowMode {
        width: 800.0,
        height: 600.0,
        maximized: false,
        fullscreen_type: conf::FullscreenType::Windowed,
        borderless: false,
        min_width: 200.0,
        max_width: 0.0,
        min_height: 200.0,
        max_height: 0.0,
        resizable: true,
    };

    let (mut ctx, mut event_loop) = ContextBuilder::new("texditor-rs", "bertino")
        .window_setup(window_conf)
        .window_mode(window_mode)
        .build()
        .expect("Shite, could not create ggez context!");

    let mut editor = Editor::new(&mut ctx, filename).expect("Could not create editor.");
    editor.init(&mut ctx);

    match event::run(&mut ctx, &mut event_loop, &mut editor) {
        Ok(_) => println!("Exited cleanly."),
        Err(e) => println!("Error occured: {}", e),
    }
}
