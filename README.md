---
title: Atestat profesional
author: Albert-Filip Geantă
---

# Introducere

Calculatoarele joacă un rol din ce în ce mai important în din ce în ce mai multe domenii ale activității umane, având în grijă sisteme de mare importanță și procesând cantități uriașe de date.

Odată cu evoluția tehnologiei, a capacității de procesare, și a nevoilor din ce în ce mai complexe, au evoluat și limbajele de programare. La început, programele erau scrise manual în instrucțiunile procesorului, apoi în limbaj de asamblare, după care au apărut limbajele de nivel înalt, începând cu C, apoi C++, Java, JavaScript etc. Fiecare dintre aceste limbaje a adus abstracțiuni din ce în ce mai sofisticate, și și-au găsit fiecare o nișă în care să fie folosite. 

Probabil cel mai cunoscut dintre ele este limbajul C, fiind și cel mai longeviv, datorită simplității sale și a aproprierii sale de limbajele de asamblare. De-a lungul timpului, diverse limbaje au încercat să înlocuiască limbajul C în aplicațiile care necesită performanță înaltă, însă au eșuat. Ultimul pretendent la această nișă este limbajul Rust, care oferă abstracțiuni și siguranța memoriei fără impact asupra performanței, cu ajutorul unui nou concept numit „Proprietate” ("Ownership"). Astfel, Rust promite performanță la același nivel ca C și siguranța memoriei ca a limbajelor de nivel mai înalt.

# Argument

Tema pe care am ales-o este unul dintre primele și cele mai utile programe, disponibile într-o formă sau alta pe orice calculator: un program de editare a textului. Am ales acest proiect după citirea unui articol care enumera proiecte pe care fiecare programator ar trebui să încerce să le implementeze. La o primă vedere, nu pare o provocare prea mare. Însă, odată ce începi să te familiarizezi cu subiectul, descoperi că în spatele unui astfel de program se află o multitudine de încercări și soluții ingenioase. 

Am ales să scriu programul în Rust ca o provocare, fiind interesat de familiarizarea cu acest nou limbaj de programare, fiind fascinat de promisiunile sale de siguranță.

Un element aparent banal cum este textul prezintă de fapt mai multe provocări, cea mai mare dintre ele fiind cea a modului de codare a caracterelor (character encoding).

Prima, și cea mai simplă, modalitate de codare a caracterelor este ASCII, un standard conceput la începutul anilor 60, care suportă 128 de caractere, reprezentate într-un singur octet, în principal litere latine ale alfabetului englez. Acestea erau suficiente înaintea apariției internetului, însă acum trebuie luate în calcul literele tuturor limbilor. Cel mai răspândit standard la momentul de față este Unicode, cu UTF-8 fiind cel mai utilizat mod de codare al caracterelor. Acesta este compatibil cu ASCII, însă îl extinde prin caractere care ocupă mai mulți octeți. Acest fapt produce o dificultate suplimentară: indexarea într-un șir de caractere nu mai este trivială, deoarece unui octet nu îi mai corespunde cu siguranță un anumit caracter.

Una dintre cele mai importante decizii care trebuie luate la un astfel de proiect este structura de date în care să fie memorat textul. Aceasta trebuie să facă față inserărilor în mijlocul șirului de caractere. Există mai multe structuri de date special concepute pentru acest lucru: „Gap Buffer”, „Rope”, „Piece Table”, fiecare cu avantajele și dezavantajele sale.

# Proceduri, funcții, obiecte, și tehnici de programare folosite

Programul este scris cu ajutorul unei biblioteci de grafică numită „ggez”, care facilitează randarea pe ecran a textului și interacțiunea utilizatorului cu programul prin evenimente.

În Rust, șirurile de caractere sunt concepute pentru UTF-8, lucru care face implementarea unui Gap Buffer mai dificilă, fiind necesară reimplementarea șirurilor de caractere. Astfel, pentru memorarea unei linii de text am conceput o structură de date inspirată, dar diferită, de Gap Buffer. Este compus dintr-un șir de caractere principal și unul secundar, denumit *buffer*, care reprezintă textul inserat în dreptul cursorului. Facilitează inserările succesive, necesitând realocarea memoriei doar după mișcarea cursorului.

Programul este compus din mai multe structuri de date, fiecare dintre ele ocupându-se de anumite lucruri:

- **Editor:** o structură de date care gestionează „contextul” grafic și randează pe ecran textul, cu metodele principale:
    - **init()** - inițializează câteva variabile care nu sunt cunoscute la construirea obiectului: lățimea unui caracter de exemplu.
    - **open_file()** - deschide și citește fișierul, trimițând apoi conținutul către următorul struct.
    - **save_file()** - scrie conținutul editat în fișier
    - **paste()** - lipește conținutul clipboard-ului la apăsarea tastelor **CTRL+V**
    - **get_mouse_pos()** - transformă coordonatele mouse-ului exprimate în pixeli în numărul de linii și coloane al caracterului deasupra căruia se află - sau numărul liniei a cărui număr a fost apăsat
    - **set_cursor()** - încearcă să mute cursorul asupra unui caracter, limitând mișcarea la lungimea liniilor și a coloanelor și, dacă este cazul, derulând conținutul ecranului în sus sau jos.
    - **draw()** - randează pe ecran textul și numărul liniilor
    - **\*\_event()** - funcții care gestionează evenimentele venite de la utilizator

- **Buffer:** o structură de date care gestionează textul în sine, gestionând un vector de *GapBuffer*-uri, un index al primei linii vizibile pe ecran. Are ca metodele principale:
    - **get_visible()** - returnează textul vizibil pentru randare
    - **scroll()** - derulează textul în sus sau jos, incrementând sau decrementând indexul primei linii vizibile
    - **delete_line()** și **new_line()** - șterg și, respectiv, creează o nouă linie.

- **GapBuffer:** o structură de date care memorează și gestionează o singură linie de text.  Are următoarele metode:
    - **insert()** - inserează un caracter
    - **remove()** - șterge un caracter 
    - **commit()** - transferă conținutul *buffer*-ului (parțial sau total) în șirul principal de caractere
    - **get_text()** - returnează textul memorat în această linie

# Prezentarea aplicației

Programul deschide un fișier al cărui nume este specificat ca prim parametru în linia de comandă, care este creat dacă nu există, sau încearcă deschiderea unui fișier prestabilit „newfile.txt”. Citește conținutul acestui fișier și îl desenează pe ecran, atașând fiecărei linii din fișier numărul corespunzător la ștanga textului său. Este desenat și un cursor, în dreptul căruia se va face inserarea caracterului apăsat de utilizator pe tastatură. Acest cursor poate fi mișcat atât prin apăsarea mouse-ului în locul dorit, cât și prin săgețile tastaturii. Derularea poate fi făcută cu ajutorul rotiței mouse-ului sau prin mutarea cursorului mai sus decât prima linie vizibilă sau mai jos de ultima. La apăsarea scurtăturii **CTRL+S**, conținutul editat este scris pe disc. La apăsarea scurtăturii **CTRL+V**, conținutul clipboard-ului este inserat în document. La apăsarea scurtăturii **CTRL+Q**, programul este oprit.

# Bibliografie

- [http://web.eecs.utk.edu/~azh/blog/challengingprojects.html](http://web.eecs.utk.edu/~azh/blog/challengingprojects.html)

- [https://www.averylaird.com/programming/the%20text%20editor/2017/09/30/the-piece-table/](https://www.averylaird.com/programming/the%20text%20editor/2017/09/30/the-piece-table/)

- [https://doc.rust-lang.org/book/https://doc.rust-lang.org/book/](https://doc.rust-lang.org/book/)
